import React from 'react'


const Header = () => {
  return (

	<section class="hero">
		<header>
    <nav class="dws-menu">
        <ul>
            <li><a href="#">Главная</a></li>
            <li><a href="#">Продукция</a>
                <ul>
                    <li><a href="#">Носки</a>
                         <ul>
                            <li><a href="#">С надписью</a></li>
                            <li><a href="#">С рисунком</a></li>
                        </ul>
                        </li>
                    <li><a href="#">Стикеры</a>
                        <ul>
                            <li><a href="#">Атака титанов</a></li>
                            <li><a href="#">Рик и Морти</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Ручки</a>
                        <ul>
                            <li><a href="#">Гелевые</a></li>
                            <li><a href="#">Шариковые</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="#">Все о оплате</a>
                <ul>
                    <li><a href="#">Услуга ( наложенный платеж )</a></li>
                    <li><a href="#">Услуга ( Предоплата ) </a></li>
                </ul>
                
            </li>
             <li><a href="#">Все о доставке</a>
                <ul>
                    <li><a href="#">По Украине</a></li>
                    <li><a href="#">Забери сам</a></li>
                </ul>
                
            </li>
            <li><a href="#">Контакты</a></li>
        </ul>
    </nav>
</header>


<section class="caption">
				<h2 class="caption">Welcome to the online-shop</h2>
			</section>
	</section>
        
  )
}

export default Header
