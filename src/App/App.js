import React from 'react'

import './common/style/main.css'
import './common/style/other.css'


import Header from './Header/Header'
import Main from './Main/Main'
import Footer from './Footer/Footer'



const App = () => {
  return (
    <>
    <Header/>
    <Main/>
    <Footer/>

	</>
  )
}

export default App
