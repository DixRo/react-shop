import React from 'react'


const Main = () => {
  return (
    
	<main class="listings">
		<div class="wrapper">
			<ul class="properties_list">
				<li>
					<a href="#">
						<img src="img/property_1.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">₴25</span>
					<div class="property_details">
						<h1 class="grid-title">Информация о товаре</h1>
                         
                     <h3>Надписи</h3> 
                        
                         
                             <h4>1) Холодно PZDC</h4>
                             <h4>2) Май English IC BEPI BEЛ</h4>
                             <h4>3) Кому надо тот и пусть</h4>
                             <h4>4) Горы по колено</h4>
                    
                        
                     <h3>Материал</h3>
                         <h4>Хлопок - 90%; Полиамид - 15%; Эластан - 5%</h4>
					</div>
				</li>
				<li>
					<a href="#">
						<img src="img/property_2.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">₴25</span>
					<div class="property_details">
						<h1 class="grid-title">Информация о товаре</h1>
                         
                     <h3>Надписи</h3> 
                         
                             
                            <h4>1) Как хочу, так и живу</h4>
                             <h4>2) 2+2=5</h4>
                             <h4>3) Вредная зая</h4>
                            <h4>-</h4>
                    
                         
                     <h3>Материал</h3>
                         <h4>Хлопок - 80%; Полиамид - 15%; Эластан - 5%</h4>
					</div>
				</li>
				<li>
					<a href="#">
						<img src="img/property_3.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">₴25</span>
					<div class="property_details">
							<h1 class="grid-title">Информация о товаре</h1>
                         
                     <h3>Надписи</h3> 
                         
                             <h4>1) Когда ты лучшая, трудно быть скромной</h4>
                             <h4>2) Хочешь ПохуДеть, спроси меня как</h4>
                             <h4>3) Якщо не зараз то коли?</h4>
                         
                        
                     <h3>Материал</h3>
                         <h4>Хлопок - 80%; Полиамид - 15%; Эластан - 5%</h4>
					</div>
				</li>
				<li>
					<a href="#">
						<img src="img/property_4.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">₴5</span>
					<div class="property_details">
						<h1 class="grid-title">Информация о товаре</h1>
                         
                     <h3>Стикеры</h3> 
                         <h4>Из мультика "Рик и Морти"</h4>
                         
                         <h3>Качество</h3>
                         <h4>Термопластичный полимер,матовое покрытие; Противостояние влаге</h4>
					</div>
				</li>
				<li>
					<a href="#">
						<img src="img/property_5.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">₴5</span>
					<div class="property_details">
						<h1 class="grid-title">Информация о товаре</h1>
                        
                     <h3>Стикеры</h3> 
                         <h4>Из мультика "Рик и Морти"</h4>
                         
                         <h3>Качество</h3>
                         <h4>Термопластичный полимер,матовое покрытие; Противостояние влаге</h4>
					</div>
				</li>
				<li>
					<a href="#">
						<img src="img/property_6.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">₴5</span>
					<div class="property_details">
						<h1 class="grid-title">Информация о товаре</h1>
                         
                     <h3>Стикеры</h3> 
                         <h4>Из мультика "Рик и Морти"</h4>
                         
                         <h3>Качество</h3>
                         <h4>Термопластичный полимер,матовое покрытие; Противостояние влаге</h4>
					</div>
				</li>
				<li>
					<a href="#">
						<img src="img/property_7.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">₴5</span>
					<div class="property_details">
						<h1 class="grid-title">Информация о товаре</h1>
                            
                     <h3>Стикеры</h3> 
                         <h4>Из аниме "Атака Титанов"</h4>
                         
                         <h3>Качество</h3>
                         <h4>Термопластичный полимер,матовое покрытие; Противостояние влаге</h4>    
					</div>
				</li>
				<li>
					<a href="#">
						<img src="img/property_8.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">₴5</span>
					<div class="property_details">
						<h1 class="grid-title">Информация о товаре</h1>
                            
                     <h3>Стикеры</h3> 
                         <h4>Из аниме "Атака Титанов"</h4>
                         
                         <h3>Качество</h3>
                         <h4>Термопластичный полимер,матовое покрытие; Противостояние влаге</h4>    
					</div>
				</li>
				<li>
					<a href="#">
						<img src="img/property_9.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">₴5</span>
					<div class="property_details">
						<h1 class="grid-title">Информация о товаре</h1>
                            
                     <h3>Стикеры</h3> 
                         <h4>Из аниме "Атака Титанов"</h4>
                        
                         <h3>Качество</h3>
                         <h4>Термопластичный полимер,матовое покрытие; Противостояние влаге</h4>    
					</div>
				</li>
                <li>
					<a href="#">
						<img src="img/property_10.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">₴15</span>
					<div class="property_details">
						<h1 class="grid-title">Информация о товаре</h1>
                          
                     <h3>Чернила</h3>
                        <h4>Cиний цвет</h4>
                          
                        <h3>Ручка</h3>
                          <h4>Гелевая</h4>
					</div>
				</li>
                <li>
					<a href="#">
						<img src="img/property_11.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">₴15</span>
					<div class="property_details">
						<h1 class="grid-title">Информация о товаре</h1>
                          
                     <h3>Чернила</h3>
                        <h4>Cиний цвет</h4>
                          
                        <h3>Ручка</h3>
                          <h4>Гелевая</h4>
					</div>
				</li>
                <li>
					<a href="#">
						<img src="img/property_12.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">₴15</span>
					<div class="property_details">
						<h1 class="grid-title">Информация о товаре</h1>
                          
                     <h3>Чернила</h3>
                        <h4>Cиний цвет</h4>
                          
                        <h3>Ручка</h3>
                          <h4>Шариковая</h4>
					</div>
				</li>
			</ul>
		</div>
	</main>	
    
  )
}

export default Main