import React from 'react'
import products from './products'

const ProductList = ({
    ProductListItem,
}) => {
    return (
        <>
            <div className="row">
                {
                    products.map(({
                        image,
                        id,
                    }) => (
                        <div className="col-lg-6" key={id}>
                            <ProductListItem
                                id={id}
                                image={image}
                            />
                        </div>
                    ))
                }
            </div>
        </>
    )
}


export default ProductList
